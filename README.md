# Gezi

* Gezi is Go library that provides a common way to access ZIP files and directories. In a way, it's a very slim version of the famous PhysFS library.
* Documentation can be found at http://godoc.org/github.com/tapir/gezi
* The name Gezi is a tribute to the 2013 pro-democracy riot of millions of Turkish people against their government who became increasingly totalitarian over the last decade.

## Usage
Let's say we have below ZIP package and directory:

```
*******************      *******************
* update.zip      *      * assets/         *
*******************      *******************
* level2.xml      *      * level1.xml      *
* images/ball.png *      * images/man.png  *
* images/hero.png *      * images/ball.png *
* audio/blink.wav *      * audio/pass.wav  *
*******************      *******************
```

We could create a new ResourceManager and add above file hierarchies to the search path:

```go
//Create a new resource manager with initial search paths
assets, _ := gezi.New("data/update.zip", "data/assets")
defer assets.Close()
```

Because of the order we have given, _update.zip_ now has the priority for the files that
have the same name. In this case:

```go
//Open ball.png from update.zip because it has priority
ball, _ := assets.OpenFile("images/ball.png")
defer ball.Close()
//Read bytes
rawBytes := ioutil.ReadAll(ball)
```

## Todo
* Test on platforms other than Linux
* Examples

