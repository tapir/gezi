package gezi

import (
	"archive/zip"
	"container/list"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type zipFile struct {
	zipReader *zip.ReadCloser
}

type dir struct {
	dirPath string
}

//ResourceManager keeps a list of directory or zip file paths.
type ResourceManager struct {
	pathList *list.List
}

//New creates a new ResourceManager and sets an initial path list. Path lists
//are prioritized. First one on the list has higher priority if there are
//multiple files with the same name.
func New(pathList ...string) (*ResourceManager, error) {
	r := &ResourceManager{list.New()}

	//Add arguments to search path
	for _, v := range pathList {
		err := r.AddPath(v, true)
		if err != nil {
			return nil, err
		}
	}

	//Success
	return r, nil
}

//Close closes open file handles.
func (r *ResourceManager) Close() {
	//Iterate through search path list
	for v := r.pathList.Front(); v != nil; v = v.Next() {
		//Check if it's a zip
		switch v.Value.(type) {
		case *zipFile:
			v.Value.(*zipFile).zipReader.Close()
		}
	}
}

//AddPath adds a new search path to the list. If end is true, the path will be
//added to the end of the list which means it will have the lowest priority when
//there are multiple files with the same name.
func (r *ResourceManager) AddPath(path string, end bool) error {
	//Get path statistics
	s, err := os.Stat(path)
	if err != nil {
		return err
	}

	//It's a directory
	if s.Mode().IsDir() {
		//Add as a directory with correct order
		if end {
			r.pathList.PushBack(&dir{path})
		} else {
			r.pathList.PushFront(&dir{path})
		}
	} else {
		//Open file as a zip
		z, err := zip.OpenReader(path)
		if err != nil {
			//Illegal zip file
			return err
		}
		//Add as a zip to pathList with correct order
		if end {
			r.pathList.PushBack(&zipFile{z})
		} else {
			r.pathList.PushFront(&zipFile{z})
		}
	}

	//Success
	return nil
}

//OpenFile opens a file if it exists in the search path list. It returns an
//io.ReadCloser interface which can be used with standard Go methods to read
//bytes into memory.
func (r *ResourceManager) OpenFile(file string) (io.ReadCloser, error) {
	//Iterate through search path list
	for v := r.pathList.Front(); v != nil; v = v.Next() {
		//Use type switch to decide path type
		switch v.Value.(type) {
		//It's a zip
		case *zipFile:
			z := v.Value.(*zipFile).zipReader
			//Search for the file in the zip
			for _, f := range z.File {
				if f.Name == file {
					//Return handle to the file found
					r, err := f.Open()
					if err != nil {
						return nil, err
					}
					return r, nil
				}
			}
		//It's a directory
		case *dir:
			d := v.Value.(*dir).dirPath
			//Create a full path of the file
			f := filepath.Join(d, file)
			//Try opening the file
			r, err := os.Open(f)
			//Check if it exist or if there is another error
			if err != nil {
				if os.IsNotExist(err) {
					break
				}
				return nil, err
			}
			return io.ReadCloser(r), nil
		}
	}

	//File not found
	return nil, fmt.Errorf("gezi: File %v not found.", file)
}
